var Heist = artifacts.require("Heist");

contract("", accounts => {
    MAIN_ACCOUNT = accounts[0];
    SECOND_ACCOUNT = accounts[0];

    it("should start", async () => {
        let heist = await Heist.deployed();

        let secretHash = "0x6131323333326100000000000000000000000000000000000000000000000000";
        
        let guessCostWei = web3.toWei(0.01, "ether");
        let hash = await heist.start(secretHash, guessCostWei);

        let playing = await heist.playing.call();
        let actualSecretHash= await heist.secretHash.call();
        let actualGuessCostWei = await heist.guessCostWei.call();
        
        assert.equal(playing, true);
        assert.equal(secretHash,  actualSecretHash);
        assert.equal(guessCostWei,  actualGuessCostWei);
    });



    it("guess and win", async () => {
        let heist = await Heist.deployed();

        let guessCode = 12212;
        let guessHash = "0x1111123333326100000000000000000000000000000000000000000000000000"

        let rightHash = "0x6131323333326100000000000000000000000000000000000000000000000000";

        let guessCostWei = await heist.guessCostWei.call();

        let initialBalance = web3.eth.getBalance(SECOND_ACCOUNT);

        await heist.tryGuess(guessCode, {from: SECOND_ACCOUNT, value: guessCostWei});  
        await heist.hack(SECOND_ACCOUNT, guessCode, guessHash);

        let after1GuessesBalance = web3.eth.getBalance(SECOND_ACCOUNT);

        await heist.tryGuess(guessCode, {from: SECOND_ACCOUNT, value: guessCostWei});  
        await heist.hack(SECOND_ACCOUNT, guessCode, guessHash);


        let after2GuessesBalance = web3.eth.getBalance(SECOND_ACCOUNT);

        await heist.tryGuess(guessCode, {from: SECOND_ACCOUNT, value: guessCostWei});  

        let contractBalance = web3.eth.getBalance(heist.address);
        console.log("before winner: contract balance: " + web3.eth.getBalance(heist.address));

        await heist.hack(SECOND_ACCOUNT, guessCode, rightHash);
        
        let endBalance = web3.eth.getBalance(SECOND_ACCOUNT);

        let guessCount = await heist.guessCount.call();
        let playing = await heist.playing.call();

        contractBalance = web3.eth.getBalance(heist.address);
        
        console.log("after winner: contract balance: " + web3.eth.getBalance(heist.address));
        console.log("initialBalance: " + initialBalance.toNumber());
        console.log("endBalance: " + endBalance.toNumber());

        assert.equal(guessCount.toNumber(), 3);
        assert.equal(playing, false);
        assert.equal(contractBalance, 0);
        assert.equal(after2GuessesBalance < initialBalance, true);
    });

});
