pragma solidity ^0.4.19;

contract Heist {
  uint public guessCostWei;
  bytes32 public secretHash;
  bool public playing;
  address public owner;

  Guess[] public guesses;

  uint public guessCount;

  struct Guess {
    address from;
    uint guessCode;
    bytes32 guessHash;
  }

  event TryGuess(address from, uint guessCode);
  event Winner(address who);

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  modifier onlyStarted() {
    require(playing);
    _;
  }

  modifier onlyStopped() {
    require(!playing);
    _;
  }

  function Heist() public {
    owner = msg.sender;
  }


  function start(bytes32 secret, uint guessWei) onlyOwner onlyStopped public {
    secretHash = secret;
    guessCostWei = guessWei;
    guessCount = 0;
    delete(guesses);
    playing = true;
  }

  function tryGuess (uint guessCode) onlyStarted payable public {
    require(msg.value == guessCostWei);
    TryGuess(msg.sender, guessCode);
  }

  function hack(address from, uint guessCode, bytes32 guessHash) onlyOwner onlyStarted public {
    Guess memory guess = Guess({
      from: from,
      guessCode: guessCode,
      guessHash:guessHash
    });
    guessCount = guesses.push(guess);
    if (guessHash == secretHash) {
      Winner(from);
      from.transfer(this.balance); //TODO: send comissions
      playing = false;
    } 
  }




}
