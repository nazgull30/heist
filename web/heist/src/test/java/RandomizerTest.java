

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import heist.Randomizer;

public class RandomizerTest {

	@Test
	public void testCreateRandomCode() {
		Randomizer randomizer = new Randomizer();
		int numberCount = 10;
		randomizer.generateRandomCode(numberCount);
		List<Integer> codeNrs = randomizer.getCodeNumbers();
		
		List<Character> codeChars = new ArrayList<>();
		for (int i = 0; i < numberCount; i++) {
			Character c = (char)codeNrs.get(i).intValue();
			codeChars.add(c);
			System.out.println(c);
		}
		
	}
}
