package heist;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Randomizer {

	List<Integer> numbers = new ArrayList<>();

	public void generateRandomCode(int numberCount) {
		for (int i = 0; i < numberCount; i++) {
			int newNr = generateNewNumber();
			numbers.add(newNr);
		}
	}

	public List<Integer> getCodeNumbers() {
		return numbers;
	}

	private int generateNewNumber() {
		Random rnd = new Random();
		int number = -1;
		while (number < 0) {

			int potentialNr = rnd.nextInt(10);

			if (!numbers.contains(potentialNr)) {
				number = potentialNr;
				break;
			}
		}
		return number;
	}
}
