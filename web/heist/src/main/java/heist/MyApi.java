package heist;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiIssuer;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;

import lombok.extern.slf4j.Slf4j;

/**
 * The Echo API which Endpoints will be exposing.
 */
// [START echo_api_annotation]
@Api(name = "echo", version = "v1", namespace = @ApiNamespace(ownerDomain = "echo.example.com", ownerName = "echo.example.com", packagePath = ""),
		// [START_EXCLUDE]
		issuers = {
				@ApiIssuer(name = "firebase", issuer = "https://securetoken.google.com/YOUR-PROJECT-ID", jwksUri = "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system"
						+ ".gserviceaccount.com") }
// [END_EXCLUDE]
)
// [END echo_api_annotation]

@Slf4j
public class MyApi {

	private static Character[] secretCode;

	@ApiMethod(name = "start")
	public Message start(@Named("numberCount") @Nullable Integer numberCount) {
		Message message = new Message();
		if (numberCount < 3 || numberCount > 10) {

			message.setMessage("numberCount should be >=3 and <= 10");
			return message;
		}
		Randomizer randomizer = new Randomizer();
		randomizer.generateRandomCode(numberCount);

		List<Integer> codeNumbers = randomizer.getCodeNumbers();
		List<Character> codeChars = new ArrayList<>();
		for (int i = 0; i < numberCount; i++) {
			Character c = (char) codeNumbers.get(i).intValue();
			codeChars.add(c);
		}
		
		secretCode = (Character[]) codeChars.toArray(new Character[codeChars.size()]);
		message.setMessage("start contract");
		return message;
	}

	private Message doEcho(Message message, Integer n) {
		if (n != null && n >= 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < n; i++) {
				if (i > 0) {
					sb.append(" ");
				}
				sb.append(message.getMessage());
			}
			message.setMessage(sb.toString());
		}
		return message;
	}
}
